﻿$(function () {


    // Build the chart
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        colors: ['lightgray', 'lightblue', 'orange', 'lightgreen'],
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Lực lượng Công an',
                y: 20,
                sliced: true,
                selected: true
            }, {
                name: 'Lực lượng PCCC',
                y: 15
            }, {
                name: 'Đội ngũ Y tế',
                y: 40
            }, {
                name: 'Lực lượng Giao thông',
                y: 25
            }]
        }]
    });
});