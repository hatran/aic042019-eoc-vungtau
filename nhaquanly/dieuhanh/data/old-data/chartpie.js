﻿$(function () {

    var chart = new Highcharts.chart({
        chart: {
            renderTo: 'container',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([37,25,20,11,16,4,5]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0,0,0,0,0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 2100);
            //    }
            //}
            //edited by LamPT
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([{
                                name: 'Trộm cắp',
                                y: 0,
                                sliced: false,
                                selected: false
                            }, {
                                name: 'Gây rối',
                                y: 0
                            }, {
                                name: 'Cố ý gây thương tích',
                                y: 0
                            }, {
                                name: 'Cướp giật',
                                y: 0
                            }, {
                                name: 'Hiếp dâm',
                                y: 0
                            }, {
                                name: 'Giết người',
                                y: 0
                            }, {
                                name: 'Vận chuyển trái phép',
                                y: 0
                            }]);
                            count = 1;
                        }
                        else if (count == 1) {
                            chart.series[0].setData([{
                                name: 'Trộm cắp',
                                y: 37,
                                sliced: false,
                                selected: false
                            }, {
                                name: 'Gây rối',
                                y: 25
                            }, {
                                name: 'Cố ý gây thương tích',
                                y: 20
                            }, {
                                name: 'Cướp giật',
                                y: 11
                            }, {
                                name: 'Hiếp dâm',
                                y: 16
                            }, {
                                name: 'Giết người',
                                y: 4
                            }, {
                                name: 'Vận chuyển trái phép',
                                y: 5
                            }]);
                            count = 2;
                        }
                        else if (count == 40) {
                            chart.series[0].setData([]);
                            count = 0;
                        }
                        else {
                            count = count + 1;
                        }
                    }, 100);
                }
            }
        },
        title: {
            text: 'BÁO CÁO 113',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Trộm cắp',
                y: 37,
                sliced: false,
                selected: false
            }, {
                name: 'Gây rối',
                y: 25
            }, {
                name: 'Cố ý gây thương tích',
                y: 20
            }, {
                name: 'Cướp giật',
                y: 11
            }, {
                name: 'Hiếp dâm',
                y: 16
            }, {
                name: 'Giết người',
                y: 4
            }, {
                name: 'Vận chuyển trái phép',
                y: 5
            }]
        }]
    });
});