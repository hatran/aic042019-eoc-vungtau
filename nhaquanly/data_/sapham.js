﻿$(function () {
    // Radialize the colors
    var chart = new Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                  [0, color],
                  [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    // Build the chart
    var chart = new Highcharts.chart('sap', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([20, 15,40,25]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0,0,0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 3000);
            //    }
            //}
            
        },
        title: {
            text: null,
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: '2014',
                y: 440,
                sliced: false,
                selected: false
            }, {
                name: '2015',
                y: 280
            }, {
                name: '2016',
                y: 200
            }, {
                name: '2017',
                y: 80
            }]
        }]
    });
});