﻿$(function () {
       Highcharts.chart('sap', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'BÁO CÁO TÌNH TRẠNG GIẢI QUYẾT Ý KIẾN PHẢN ÁNH CỦA NGƯỜI DÂN'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Đã giải quyết',
                y: 100,
                sliced: false,
                selected: false
            }, {
                name: 'Đang giải quyết',
                y: 20
            }, {
                name: 'Chưa giải quyết',
                y: 5
            }]
        }]
    });
});